package claudio.mamani.callcenter.fixture;

import java.util.ArrayList;

import claudio.mamani.callcenter.employee.Director;
import claudio.mamani.callcenter.employee.Employee;
import claudio.mamani.callcenter.employee.Operator;
import claudio.mamani.callcenter.employee.Supervisor;
import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.enumemployee.TypeEmployee;

public class EmployUtil {
	
	private final int numTypeEmployee=3;
	private ArrayList<Employee>[] typeEmployeeList;
	private ArrayList<Employee> EmployeeOperatorList;
	private ArrayList<Employee> EmployeeSupervisorList;
	private ArrayList<Employee> EmployeeDirectorList;
	
	
	public EmployUtil() {
		
	}
	
	/*
	 * This method generate a list with diferents types of employees.
	 * @author: Claudio Mamani
	 * @return a type employees list.
	 * 
	 * */
	public ArrayList<Employee>[] getListEmploys() {			
		
		this.typeEmployeeList=new ArrayList[numTypeEmployee];
		this.EmployeeOperatorList=new ArrayList<Employee>();
		this.EmployeeSupervisorList=new ArrayList<Employee>();
		this.EmployeeDirectorList=new ArrayList<Employee>();
			
		for (int i = 0; i < 10; i++) {
			if(i<5) {
				this.EmployeeOperatorList.add(new Operator(StatusEmployee.AVAILABLE));
			}else if(i<8) {
				this.EmployeeSupervisorList.add(new Supervisor(StatusEmployee.AVAILABLE));
			}else
				this.EmployeeDirectorList.add(new Director(StatusEmployee.AVAILABLE));
		}
		
		this.typeEmployeeList[0]=this.EmployeeOperatorList;
		this.typeEmployeeList[1]=this.EmployeeSupervisorList;
		this.typeEmployeeList[2]=this.EmployeeDirectorList;
		
		return this.typeEmployeeList;
	
		
	}

}
