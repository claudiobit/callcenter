package claudio.mamani.callcenter;


import java.util.ArrayList;
import java.util.Random;
import claudio.mamani.callcenter.employee.Employee;
import claudio.mamani.callcenter.enumemployee.StatusEmployee;



public class Dispatcher extends Thread {
	
	
	private ArrayList<Employee>[] typeEmployeeList;
	private Employee currentEmp;
	private boolean callwaiting;
	
	

	public Dispatcher( ArrayList<Employee>[] typeEmployeeList) {
		super();		
		this.typeEmployeeList=typeEmployeeList;
		this.callwaiting=false;
		
	}
	
	/*
	 * This method get an available employee.
	 * @author: Claudio Mamani
	 * @return available employee or null if not exist.
	 * 
	 * */	
	public synchronized Employee getEmployee() {
		for (int i = 0; i < typeEmployeeList.length; i++) {
			ArrayList<Employee> empList=typeEmployeeList[i];
			for (Employee employee : empList) {
				if(employee.isAvailable())
					return employee;
			}
		}
		
		return null;
	}
	
	
	/*
	 * This method update status of employee.
	 * @author: Claudio Mamani
	 * @param Employee
	 * 
	 * */
	public synchronized void updateStatusEmployee(Employee currentEmp) {
		for (int i = 0; i < typeEmployeeList.length; i++) {
			ArrayList<Employee> empList=typeEmployeeList[i];
			if(empList.contains(currentEmp)) {
				int index=0;
				for (int j = 0; j < empList.size(); j++) {
					if(empList.get(j).equals(currentEmp)) {
						index=j;
						break;
					}						
				}
				
				if(currentEmp.getStatus().equals(StatusEmployee.AVAILABLE)) {					
					currentEmp.setStatus(StatusEmployee.BUSY);					
				}else {					
					currentEmp.setStatus(StatusEmployee.AVAILABLE);					
				}										
				
				empList.add(index, currentEmp);
			}
		}
		
	}
	
	
	/*
	 * This method assign calls to available employee, if there is not available employee
	 * then change flag callWaiting to true.
	 * @author: Claudio Mamani
	 * @param Call
	 * 
	 * */
	public synchronized void distpatchCall(Call call) {		
	
		this.currentEmp=getEmployee();
		if(this.currentEmp!=null) {			
			this.currentEmp.receiveCall(call);
			updateStatusEmployee(this.currentEmp);
		}else {
			this.callwaiting=true;
		}
			
	}
	
	
	@Override
	public void run() {
		try {
			
			Random rnd = new Random();			
			distpatchCall(new Call());			
			Thread.sleep(rnd.nextInt(10000));
			updateStatusEmployee(this.currentEmp);			
			
			
		} catch (Exception e) {
			System.out.println("Dispatcher run error: "+e);
		}
		
		
	
	}
	
	public boolean getCallWaiting() {
		return this.callwaiting;
	}
	
		
	

}
