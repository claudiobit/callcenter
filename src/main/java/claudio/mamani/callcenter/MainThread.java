package claudio.mamani.callcenter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import claudio.mamani.callcenter.employee.Employee;
import claudio.mamani.callcenter.fixture.EmployUtil;

public class MainThread extends Thread{
	private EmployUtil empUtil;
	private ArrayList<Employee>[] typeEmployeeList;
	private  Queue<Call> queueCall=new LinkedList<Call>();;
	private int numCalls;
	private final int maxEmployee=10;//numbers available employees;
	private boolean incomingCalls;
	
	/*
	 * This constructor receive a number that represents the number of calls
	 * incoming and obtains a list of employees. 
	 * @Autor Claudio Mamani
	 */
	public MainThread(int numCalls) {
		this.numCalls=numCalls;
		this.empUtil=new EmployUtil();
		this.typeEmployeeList=this.empUtil.getListEmploys();
		addQueueCalls();
	}
	
	
	/*
	 * This method add calls to queueCalls if the numCall is mayor than 10
	 * and assing true to the flag incomingCall.
	 * @Autor Claudio Mamani
	 */
	public void addQueueCalls() {
		if(this.numCalls>10) {
			int resultado=numCalls-this.maxEmployee;
			this.numCalls=this.maxEmployee;
			for (int i = 0; i < resultado; i++) {
				this.queueCall.add(new Call());
			}								
		}
		
		this.incomingCalls=true;
		
	}
	
	/*
	 * This method get calls from queueCalls and update queueCalls,
	 * then the calls are again incoming. 
	 * @Autor Claudio Mamani
	 */
	public void updateQueueCalls() {
		 this.numCalls=0;					
		 int size=this.queueCall.size();
		 for (int j = 0; j < size; j++) {						   
			   this.queueCall.poll();
			   this.numCalls++;						   
		 }					 
		 addQueueCalls();
		
	}
	
	/*
	 * This method execute the thread until queueCalls is less than 0 or
	 * the flag incomingCall is false.
	 * then the calls are again incoming. 
	 * @Autor Claudio Mamani
	 */
	
	public void run() {
		try {
			   while(this.queueCall.size()>0 || this.incomingCalls) {				   
				   for (int i = 0; i < this.numCalls; i++) {				
						new Dispatcher(this.typeEmployeeList).start();							
					}
				   
				   if(this.queueCall.size()>0) {
					   Thread.sleep(10000);
					   updateQueueCalls();					   
				   }else {
					   this.incomingCalls=false;
				   }
				   
			   }		
			
			
		} catch (Exception e) {
			System.out.println("MainThread run error: "+e);
		}
		
		
	}

	public Queue<Call> getQueueCall() {
		return queueCall;
	}

	public void setQueueCall(Queue<Call> queueCall) {
		this.queueCall = queueCall;
	}
	
	
	
	

}
