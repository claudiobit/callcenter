package claudio.mamani.callcenter.employee;

import claudio.mamani.callcenter.Call;
import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.enumemployee.TypeEmployee;

public class Employee {

	//Put here common fields
	private StatusEmployee status;
	protected TypeEmployee typeEmployee;
	
	
	public Employee() {
		
	}
	

	public Employee(StatusEmployee statusEmployee) {
		super();		
		this.status=statusEmployee;
		
	}




	public StatusEmployee getStatus() {
		return status;
	}


	public void setStatus(StatusEmployee status) {
		this.status = status;
	}


	public TypeEmployee getTypeEmployee() {
		return typeEmployee;
	}


	public void setTypeEmployee(TypeEmployee typeEmployee) {
		this.typeEmployee = typeEmployee;
	}

	public boolean isAvailable() {
		return this.status==StatusEmployee.AVAILABLE;
	}
	
	public void receiveCall(Call call) {
		
		System.out.println("==============================================================");
		System.out.println("Type of employee who received the call: "+this.typeEmployee);
		System.out.println("==============================================================");
		
	}


	
	

	
}
