package claudio.mamani.callcenter.employee;

import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.enumemployee.TypeEmployee;

public class Director extends Employee {
	// Put here all specific properties and methods.
	public Director() {
		
	}

	public Director(StatusEmployee statusEmployee) {
		super(statusEmployee);
		this.typeEmployee = TypeEmployee.DIRECTOR;
		
	}

}
