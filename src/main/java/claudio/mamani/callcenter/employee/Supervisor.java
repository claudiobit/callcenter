package claudio.mamani.callcenter.employee;

import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.enumemployee.TypeEmployee;

public class Supervisor extends Employee {

	// Put here all specific properties and methods.
	public Supervisor() {
		
	}

	public Supervisor(StatusEmployee statusEmployee ) {
		super(statusEmployee);
		this.typeEmployee = TypeEmployee.SUPERVISOR;
	}

}
