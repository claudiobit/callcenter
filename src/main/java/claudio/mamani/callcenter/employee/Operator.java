package claudio.mamani.callcenter.employee;

import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.enumemployee.TypeEmployee;

public class Operator extends Employee {
	// Put here all specific properties and methods.
	
	
	public Operator() {
		
	}

	public Operator(StatusEmployee statusEmployee) {
		super(statusEmployee);
		this.typeEmployee = TypeEmployee.OPERATOR;
		 
	}

}
