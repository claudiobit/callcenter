package claudio.mamani.callcenter.mainapplication;




import claudio.mamani.callcenter.MainThread;


public class App 
{
    public static void main( String[] args )
    {
    	//This variable simulate the number of calls incoming.
    	int numCallsSimulation=10;
    	MainThread mt=new MainThread(numCallsSimulation);
    	//This will show by console what type of employee receive the call.
    	mt.start();   	
    	
    	
    }

	
}
