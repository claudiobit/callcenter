package claudio.mamani.callcenter.enumemployee;

public enum TypeEmployee {
	OPERATOR,
	SUPERVISOR,
	DIRECTOR

}
