package claudio.mamani.callcenter;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import claudio.mamani.callcenter.employee.Employee;
import claudio.mamani.callcenter.enumemployee.StatusEmployee;
import claudio.mamani.callcenter.fixture.EmployUtil;

public class DispatcherTest {
	
	private Dispatcher dispatcher;
	private EmployUtil empUtil;
	private Employee emp;
	
	/*
	 * this method generate data for test
	 * 
	 * */
	@Before
	public void before() {
		this.empUtil=new EmployUtil();
		this.dispatcher=new Dispatcher(this.empUtil.getListEmploys());		
		this.emp=this.dispatcher.getEmployee();
	}	
	
	
	/*
	 * This method check status employee change correctly, this is used 
	 * after dispatchCall is executed
	 * 
	 * */
	@Test
	public void updateStatusEmployeeTest() {		
		
		this.dispatcher.updateStatusEmployee(this.emp);		
		assertEquals(StatusEmployee.BUSY, this.emp.getStatus());
		
		this.dispatcher.updateStatusEmployee(this.emp);
		assertEquals(StatusEmployee.AVAILABLE, this.emp.getStatus());
	}
	
	/*
	 * This method assign call to an employee, and check that status 
	 * employee is equal to StatusEmployee.BUSY
	 * 
	 * */
	@Test
	public void dispatchCallTest() {
		this.dispatcher.distpatchCall(new Call());		
		assertEquals(StatusEmployee.BUSY, this.emp.getStatus());
		
	}
	
	
	/*
	 * This method execute ten calls at the same time.
	 * and check the flag callWaiting for determine that all were executed,
	 * if callWaiting is false all calls were executed.
	 * */
	@Test
	public void dispatchTenCallTest() {		
		for (int i = 0; i < 10; i++) {			
			this.dispatcher.distpatchCall(new Call());
			assertEquals(false, this.dispatcher.getCallWaiting());
		}
			
		
	}
	
	/*
	 * This method execute more than ten calls at the same time.
	 * and check the flag callWaiting for determine that all  were executed,
	 * if callWaiting is true, then there are calls in the queueCall.  
	 * 
	 * */
	
	@Test
	public void dispatchMoreThanTenCallTest() {		
		
		for (int i = 0; i < 14; i++) {			
			this.dispatcher.distpatchCall(new Call());
			assertEquals(true, this.dispatcher.getCallWaiting());
		}	
		
	}




}
